import { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./App.scss";

class App extends Component {

  state = {
    characterList: [],
  };

  componentDidMount() {
    fetch('https://rickandmortyapi.com/api/character/')
    .then(res => res.json())
    .then(res => {
      if (res && !res.error) this.setState({ characterList: res.results});
    })
    .catch(error => console.log(error));
  };

  render() {
    return (
      <Router>
      <div className="app">
        <Link to="/lolo">Ir a lolo</Link>
        <Link to="/register">Ir al registro</Link>
        <h1>Listado de personajes</h1>

        {/* Ruteo */}
        <Switch>
          <Route path="/register" exact>
            <h1>Esto sería el registro</h1>
          </Route>
          <Route path="/lolo" exact>
            <h1>Este es el componente Lolo</h1>
          </Route>
        </Switch>
        <div>
          {this.state.characterList.length > 0
            ? (
              this.state.characterList.map(character => {
                return (
                  <div key={character.id}>
                    <h3>ID: {character.id}</h3>
                    <h3>Name: {character.name}</h3>
                    <img src={character.image} alt="character" />
                    <p>Status: {character.status}</p>
                    <p>Species: {character.species}</p>
                  </div>
                )
              })
            )
            : <h3>No hay personajes cargados!</h3>
          }
        </div>
      </div>
      </Router>
    ); 
  }
}

export default App;
